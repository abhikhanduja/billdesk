//
//  ViewController.swift
//  BillDesk
//
//  Created by abhinav khanduja on 10/02/20.
//  Copyright © 2020 abhinav khanduja. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    func billDeskScreen() {
        let msg = "ABCD|T12345|NA|500.00|NA|NA|NA|INR|NA|R|abcd|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|h ttp://www.domain.com/response.jsp|3712345678"
        
        let token = "ABCD|T12345|NA|500.00|NA|NA|NA|INR|NA|R|abcd|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|h ttp://www.merchantdomain.com/billdesk/pg_resp.php|1480699352|CP1005!ABCD!12A6F E4478DD83BC927437FEE582A0B826C5439294E0333D6251E2A1E88A42E53E214C6F 99CB31493683FF79FED2A2D9!NA!NA!NA"
        
        guard let vc = BDViewController(message: msg, andToken: token, andEmail: "some@email.com", andMobile: "9123456789") else {return}
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @IBAction func btnTapped(_ sender: Any) {
        billDeskScreen()
    }
}

